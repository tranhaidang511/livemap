
/* 表示限度数 */
var limit = 1000;

var showtimefrom = 0;
var onreload = false;
/* ポスト閲覧 */
var showRecentPosts = function(){
	onreload = true;
	if(showtimefrom==0){
		$.getJSON("/userpage/getPosts_ajax?from="+(new Date().getTime()-10*60*1000), function(json){
			clearPosts(json.posts, json.users);
			if(json.posts.length==0){
			} else {
				showtimefrom = json.posts[json.posts.length-1].created;
			}
			onreload = false;
		});
	} else {
		$.getJSON("/userpage/getPosts_ajax?from="+showtimefrom, function(json){
			addPosts(json.posts, json.users);
			if(json.posts.length==0){
			} else {
				showtimefrom = json.posts[json.posts.length-1].created;
			}
			onreload = false;
		});
	}
};

var max = -1;
var max_zIndex = -1;
var loop = false;

var clearPosts = function(posts, users){
	var m = max;
	if(loop){
		m = limit;
	}
	for(var i = 0; i < m; i++){
		if(marker[i]){
			marker[i].setMap(null);
		}
	}
	max = -1;
	loop = false;

	addPosts(posts, users);
}

var addPosts = function(posts, users){
	for(var i = 0; i < posts.length; i++){
		addPost(posts[i], users[i])
	}
}

var addPost = function(post, user){
	max = max +1;
	if(max >= limit){
		max -= limit;
		loop = true;
	}


	var weather = (post.genre == '1');
	if(weather){
		var icon = "/public/images/genre/weather"+post.message+".gif";
		marker[max] = new google.maps.Marker({
			position: new google.maps.LatLng(post.gps_lat, post.gps_lng),
			icon: new google.maps.MarkerImage(
							icon,
						    new google.maps.Size(50,61), // size
						    new google.maps.Point(0,0),  // origin
						    new google.maps.Point(26,61) // anchor
							),
			map: map
		});
		return;
	}

	var icon = "/public/images/genre/"+post.genre+".png";
	marker[max] = new google.maps.Marker({
		position: new google.maps.LatLng(post.gps_lat, post.gps_lng),
		icon: new google.maps.MarkerImage(
						icon,
					    new google.maps.Size(30,54), // size
					    new google.maps.Point(0,0),  // origin
					    new google.maps.Point(15,45) // anchor
						),
		map: map
	});

	/* ポスト内容 */
	info[max] = new google.maps.InfoWindow();

	max_zIndex++;
	info[max].setZIndex(max_zIndex);

	var imgurl;
	if(user.image.UUID){
		imgurl = '/attachments/'+user.image.UUID;
	} else {
		imgurl = '/public/images/profile/default.jpg';
	}

	var post_html =
		'<div class="clearfix">'+
		'<div class="post_side">'+
		'<a href="/user/?uid='+user.id+'">'+
		'<img src="'+imgurl+'" width="30px" height="30px" class="profile"/>'+
		'</a>'+
		"</div>"+
		'<div class="post_main">'+
		'<div class="post_name">'+
		'<a href="/user/?uid='+user.id+'">'+user.userid+'</a>' +' - '+user.name+
		'</div>'+
		'<div class="post_post">'+
		escapeHtml(post.message)+
		'</div>'+
		'<div class="post_date">'+
		millisToDate(post.created)+
		'</div>'+
		'</div>';
	info[max].setContent(post_html);
	info[max].open(map, marker[max]);

	var this_num = max;
	google.maps.event.addListener(marker[this_num], "click", function() {
		info[this_num].open(map, marker[this_num]);
		max_zIndex++;
		info[this_num].setZIndex(max_zIndex);
	});
}

/**
 * GPS機能
 * @author ALPEN
 */

$('#start_gps').click(function(){
	navigator.geolocation.watchPosition(
		function(position){
			$('#gps_lat').val(position.coords.latitude); //緯度
			$('#gps_lng').val(position.coords.longitude); //経度
			if(!cross){
				cross = new google.maps.Marker({
					position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
					icon: new google.maps.MarkerImage(
							"/public/images/crosshair.png",
						    new google.maps.Size(60,60), // size
						    new google.maps.Point(0,0),  // origin
						    new google.maps.Point(10,10) // anchor
						  )
				});
				cross.setMap(map);
			} else {
				cross.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
			}
		}
	);
});

/** Menu **/
function MapContextMenu(map) {

	this.map_ = map;
	this.setMap(this.map_);
	this.lat_ = this.map_.getCenter().lat();
	this.lng_ = this.map_.getCenter().lng();

}

MapContextMenu.prototype = new google.maps.OverlayView();

MapContextMenu.prototype.onAdd = function(){

	this.createMenu_();
	this.stopEvent();

	var panes = this.getPanes();
	panes.floatPane.appendChild( this.menu_ );

	this.MenuVisibleOff();

	var that = this;

	google.maps.event.addListener(map, "click", function() {
		that.MenuVisibleOff();
	});
	google.maps.event.addListener(map, "rightclick", function(event) {
		that.lat_ = event.latLng.lat();
		that.lng_ = event.latLng.lng();
		that.MenuVisibleOn(event.latLng,0);
	});
	google.maps.event.addListener(map, "mouseout", function() {
		that.MenuVisibleOff();
	});

}

MapContextMenu.prototype.draw = function() {
}

MapContextMenu.prototype.createMenu_ = function() {

	this.menu_ = document.createElement("ul");
	this.menu_.style.listStyle = "none";
	this.menu_.style.position = "absolute";
	this.menu_.style.backgroundColor = "#ffffff";
	this.menu_.style.color = "#000000";
	this.menu_.style.width = "200px";
	this.menu_.style.fontSize = "12px"
		this.menu_.style.padding = "0px 0px";
	this.menu_.style.margin = "0px";
	this.menu_.style.border = "1px solid #cccccc";

	var cross;
	var geocoder = new google.maps.Geocoder();
	if($("#gps_lat").val()!='' && $("#gps_lng").val()!=''){
	var latlng = new google.maps.LatLng($("#gps_lat").val(), $("#gps_lng").val());
		$("#gps_vis").html('設定済');
		if (geocoder) {
			geocoder.geocode({'latLng': latlng}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[2]) {
						$("#gps_vis").html('設定済 ('+results[2].formatted_address+')');
					}
				}
			});
		}

		if(!cross){
			cross = new google.maps.Marker({
				position: latlng,
				icon: new google.maps.MarkerImage(
						"/public/images/crosshair.png",
						new google.maps.Size(60,60), // size
						new google.maps.Point(0,0),  // origin
						new google.maps.Point(10,10) // anchor
				)
			});
			cross.setMap(map);
		} else {
			cross.setPosition(latlng);
		}
	}

	this.addListItem_(this.menu_,"ここを現在位置に指定",function(mouse){
		$("#gps_lat").val(mouse.lat_);
		$("#gps_lng").val(mouse.lng_);

		var latlng = new google.maps.LatLng(mouse.lat_, mouse.lng_);


		$("#gps_vis").html('設定済');
		if (geocoder) {
			geocoder.geocode({'latLng': latlng}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[2]) {
						$("#gps_vis").html('設定済 ('+results[2].formatted_address+')');
					}
				}
			});
		}

		if(!cross){
			cross = new google.maps.Marker({
				position: new google.maps.LatLng(mouse.lat_, mouse.lng_),
				icon: new google.maps.MarkerImage(
						"/public/images/crosshair.png",
						new google.maps.Size(60,60), // size
						new google.maps.Point(0,0),  // origin
						new google.maps.Point(10,10) // anchor
				)
			});
			cross.setMap(map);
		} else {
			cross.setPosition(new google.maps.LatLng(mouse.lat_, mouse.lng_));
		}
	});
	/*
       this.addSeparator_(this.menu_,"separator1");*/
}

MapContextMenu.prototype.addListItem_ = function(menu,myTxt,myAction,myVisible) {

	var listitem = document.createElement("li");
	listitem.style.padding = "5px 10px 5px 10px";
	listitem.appendChild(document.createTextNode(myTxt));

	var that = this;

	google.maps.event.addDomListener(listitem, "mouseover", function() {
		listitem.style.cursor = "pointer";
		listitem.style.backgroundColor = "#ffff99";
	});
	google.maps.event.addDomListener(listitem, "mouseout", function() {
		listitem.style.cursor = "default";
		listitem.style.backgroundColor = "#ffffff";
	});

	google.maps.event.addDomListener(listitem, "click", function() {
		if(myAction)myAction(that);
		that.MenuVisibleOff();
	});
	menu.appendChild(listitem);
}

MapContextMenu.prototype.addSeparator_ = function(menu,myID,myVisible) {

	var line = document.createElement("li");
	line.style.margin = "3px";
	line.style.borderBottom = "1px solid #eeeeee";
	menu.appendChild(line);

}

MapContextMenu.prototype.setMenuPosition_ = function(myLocation) {

	// 緯度、経度の情報を、Pixelに変換
	var point = this.getProjection().fromLatLngToDivPixel(myLocation);

	if (point.x + this.menu_.offsetWidth > this.map_.getDiv().offsetWidth) {
		point.x = point.x - this.menu_.offsetWidth;
	}
	if (point.y + this.menu_.offsetHeight > this.map_.getDiv().offsetHeight) {
		point.y = point.y - this.menu_.offsetHeight;
	}

	this.menu_.style.left = point.x  + 'px';
	this.menu_.style.top = point.y  + 'px';
}

MapContextMenu.prototype.MenuVisibleOff = function () {
	this.menu_.style.visibility = "hidden";
};

MapContextMenu.prototype.MenuVisibleOn = function (myLocation,markerFlg) {
	this.menu_.style.visibility = "visible";
	this.setMenuPosition_(myLocation);
}

MapContextMenu.prototype.stopEvent = function() {
	var events = ["click", "dblclick", "rightclick", "mousedown", "mouseup",
	              "mousemove", "mouseover", "mouseout", "dragstart", "dragend", "contextmenu"];
	var menu = this.menu_;

	for (var i = 0, event; event = events[i]; i++) {
		google.maps.event.addDomListener(menu, event, function(e) {
			e.cancelBubble = true;      //IE
			if (e.stopPropagation) {
				e.stopPropagation();     //Firefox
			}
		})
	}
};