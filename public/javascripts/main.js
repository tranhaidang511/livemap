/* exp4main js */
var escapeHtml = function(val) {
	return $("<div />").text(val).html();
};

var millisToDate = function(millis){
	var date = new Date();
	date.setTime(millis);

	return date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
}