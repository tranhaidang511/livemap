package controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;

import play.mvc.Controller;
import models.Post;
import models.User;
import service.UserService;
import utils.LoginUtils;
import forms.PostForm;
import play.data.validation.Valid;
import play.db.jpa.Blob;

public class Userpage extends Controller{
	/*
	 * ユーザーページ設定
	 */
	public static void index() {		// TOPページ
		User entry = LoginUtils.check(session);
		if(entry==null){
			Application.index();
		}
        render(entry);
    }

	public static void getPosts_ajax(String from){
		List<Post> posts = Post.find("created > ?", Long.valueOf(from)).fetch(1000);
		List<models.User> users = new ArrayList<models.User>();
		for(Post p : posts){
			users.add((models.User)models.User.findById(p.author));
		}

		HashMap<String, Object> ret = new HashMap<String, Object>();
		ret.put("posts", posts);
		ret.put("users", users);
		renderJSON(ret);
	}

	public static void post_ajax(@Valid PostForm form){	// ポスト機能
		User entry = LoginUtils.check(session);
		if(entry==null){
			Application.index();
		}
		if (validation.hasErrors()){
			HashMap ret = new HashMap<String, String>();
			ret.put("status", "fail");
			renderJSON(ret);
		}
		Post post = new Post(entry.id, form.message, Double.parseDouble(form.gps_lat), Double.parseDouble(form.gps_lng), Integer.parseInt(form.genre));
		post.save();
		HashMap ret = new HashMap<String, String>();
		ret.put("status", "success");
		renderJSON(ret);
	}

	public static void search(String keyword){
		if(keyword == null){
			index();
		}
		keyword = keyword.replaceAll("\n", " ");
		keyword = keyword.replaceAll("\r", " ");
		keyword = keyword.replaceAll("%", " ");
		keyword = keyword.trim();
		if(keyword.isEmpty()){
			index();
		}

		keyword = "%"+keyword+"%";
		List<models.Post> posts = models.Post.find("message LIKE ? AND genre <> 1 order by created desc", keyword).fetch(1000);
		List<PostAndUser> infos = new ArrayList<PostAndUser>(posts.size());
		for(Post p : posts){
			infos.add(new PostAndUser(p, (models.User)models.User.findById(p.author)));
		}
		render(infos);
	}

	public static class PostAndUser {
		public Post post;
		public User user;
		public PostAndUser (Post post, User user){
			this.post = post;
			this.user = user;
		}
	}
}
