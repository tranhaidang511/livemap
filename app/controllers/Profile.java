package controllers;

import org.apache.commons.codec.digest.DigestUtils;

import forms.ProfileForm;
import models.User;
import play.data.validation.Valid;
import play.mvc.Controller;
import service.UserService;
import utils.LoginUtils;

public class Profile extends Controller {

  /*
   * プロフィール設定
   */

  public static void index() {		// プロフィール編集
    models.User entry = LoginUtils.check(session);
    if(entry==null){
      Application.index();
    }
    if(entry==null){
      render("usernotfound.html");
    }
    models.User user = entry;
    render(entry, user);
  }

  public static void post(@Valid ProfileForm form){	// プロフィール送信
    models.User user = LoginUtils.check(session);
    models.User entry= user;
    if(validation.hasErrors()){
      render("@index", entry, user);
    }

    user.name = form.name;
    user.profile = form.profile;
    if(form.image != null){
      user.image = form.image;
    }
    if(form.password !=null && !form.password.isEmpty()){
      user.password = DigestUtils.md5Hex(form.password);
    }
    user.save();
    controllers.User.index(new Long(user.getId()).toString());
  }
}
