package controllers;

import java.util.HashMap;
import java.util.Map;

import models.User;

import org.apache.commons.codec.digest.DigestUtils;

import forms.RegisterForm;

import play.data.validation.Valid;
import play.db.jpa.Blob;
import play.mvc.Controller;
import service.UserService;
import utils.LoginUtils;

/*
 * 登録画面設定
 */
public class Register extends Controller {

	public static void index() {		// ユーザー登録ページ
        render();
    }

	public static void post(@Valid RegisterForm form) {		// 新規ユーザー情報をデータベースへ登録
	  if(validation.hasErrors()){
	    render("Register/index.html");
	  }

		User entry = new User(form.id, DigestUtils.md5Hex(form.password), null, form.id, "");
		entry.save();
		LoginUtils.login(session, form.id);
        render(entry);
    }

	public static void register_json(String id){			// Javascript有効の人向け
		User entry = UserService.findByUserid(id);
		Map<String,	Object>	result = new HashMap<String, Object>();
		if(entry != null){	// 重複していた場合
			result.put("id", "true");
		}else{							// 重複していない場合
			result.put("id", "false");
		}
		renderJSON(result);
	}

}
