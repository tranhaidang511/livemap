package controllers;

import java.util.List;

import play.mvc.Controller;
import service.UserService;
import utils.LoginUtils;

public class User extends Controller{
  //http://localhost:9000/user/?uid=12
  public static void index(String uid){
    models.User entry = LoginUtils.check(session);
    if(entry==null){
      Application.index();
    }
      models.User user = UserService.findByUid(uid);
      if(user==null){
        render("/user/usernotfound.html", entry);
      }
      List<models.Post> myposts = models.Post.find("author=? AND genre <> 1 order by created desc", Long.parseLong(uid)).fetch();
      render(entry, user, myposts);
  }
}
