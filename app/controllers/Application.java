package controllers;

import play.data.validation.Valid;
import play.mvc.Controller;
import service.UserService;
import utils.LoginUtils;

import java.util.*;
import models.*;

import org.apache.commons.codec.digest.DigestUtils;

import forms.LoginForm;

/**
 * ログインページ
 * @author omura
 *
 */
public class Application extends Controller {

	public static void index() {		// ログインページ
		if(LoginUtils.check(session)!=null){
			Userpage.index();
		}
		LoginForm form = new LoginForm();
        render(form);
    }

	public static void login(@Valid LoginForm form) {		// ユーザー認証
		if(validation.hasErrors()){
			render("@index", form);
		}
		LoginUtils.login(session, form.id);
		Userpage.index();
    }

	public static void logout(){
		LoginUtils.logout(session);
		index();
	}

}