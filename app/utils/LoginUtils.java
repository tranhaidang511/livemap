package utils;


import models.User;
import play.mvc.Scope.Session;
import service.UserService;

public class LoginUtils {
	private LoginUtils(){
	}

	public static String uid = "uid";

	public static void login(Session session, String userid){
    	models.User user = UserService.findByUserid(userid);
		login(session, user);
	}

	public static void login(Session session, User user){
		session.put(uid, user.getId());
	}

	public static User check(Session session){
		if(!session.contains(uid) || session.get(uid) == null){
			return null;
		}
		return User.findById(Long.valueOf(session.get(uid)));
	}

	public static void logout(Session session){
		session.remove(uid);
	}
}
