package models;

import javax.persistence.MappedSuperclass;

import play.db.jpa.Model;

@MappedSuperclass
public abstract class AbstractModel extends Model {
	public Long modified = System.currentTimeMillis();
	public Long created = modified;
}
