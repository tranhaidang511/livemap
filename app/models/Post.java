package models;

import javax.persistence.Entity;

import codes.GenreCode;

@Entity
public class Post extends AbstractModel {

	/**
	 * 投稿したUserのID
	 */
	public long author;
	/**
	 * GPS情報
	 */
	public double gps_lat;
	public double gps_lng;

	/**
	 * 本文
	 */
	public String message;

	/**
	 * ジャンルコード
	 * @see codes.GenreCode
	 */
	public int genre = GenreCode.NONE;
	
	
	public Post(long author, String message, double gps_lat, double gps_lng,
			int genre) {
		super();
		this.author = author;
		this.gps_lat = gps_lat;
		this.gps_lng = gps_lng;
		this.message = message;
		this.genre = genre;
	}
	public Post(long author, String message, double gps_lat, double gps_lng) {
		super();
		this.author = author;
		this.gps_lat = gps_lat;
		this.gps_lng = gps_lng;
		this.message = message;
	}

}
