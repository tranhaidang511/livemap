package models;

import play.*;
import play.db.jpa.*;

import javax.persistence.*;
import java.util.*;

@Entity
public class User extends AbstractModel {
  public String userid;
  public String password;
  public Blob image;
  public String name;
  public String profile;

  public User(String userid, String password, Blob image, String name, String profile) {
    this.userid = userid;
    this.password = password;
    this.image = image;
    this.name = name;
    this.profile = profile;
  }
}