package codes;

public class GenreCode {
	public final static int NONE = 0;
	public final static int WEATHER = 1;
	public final static int EVENT = 2;
	public final static int REVIEW = 3;
}
