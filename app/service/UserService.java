package service;

import models.User;

public class UserService {
	private UserService(){
	}

	public static User findByUserid(String userid){
		return User.find("userid=?", userid).first();
	}
	public static User findByUid(String uid){
	  Long id;
	  try{
	    id = Long.parseLong(uid);
	  } catch(Exception e){
	    return null;
	  }
	  return User.findById(id);
	}
}
