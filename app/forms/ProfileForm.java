package forms;

import play.data.validation.Check;
import play.data.validation.CheckWith;
import play.data.validation.Equals;
import play.data.validation.Match;
import play.data.validation.MinSize;
import play.data.validation.Required;
import play.db.jpa.Blob;

public class ProfileForm{
  public String name;
  public String profile;

  @Equals("passwordConfirmation")
  @Match("[\\.\\_\\-a-zA-Z0-9]*")
  @CheckWith(ValidPassword.class)
  public String password;

  public String passwordConfirmation;
  public Blob image;


  private static class ValidPassword extends Check {

    @Override
    public boolean isSatisfied(Object validatedObject, Object value){
      String strval = (String)value;
      if(strval != null && !strval.isEmpty()){
        return strval.length() >= 3;
      }
      return true;
    }

  }
}
