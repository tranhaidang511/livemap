package forms;

import org.apache.commons.codec.digest.DigestUtils;

import models.User;
import play.data.validation.Check;
import play.data.validation.CheckWith;
import play.data.validation.Required;
import service.UserService;

public class LoginForm {
	@CheckWith(ValidUser.class)
	@Required
	public String id;
	@Required
	public String password;

	private static class ValidUser extends Check{
		@Override
		public boolean isSatisfied(Object validatedObject, Object value) {
			LoginForm form = (LoginForm)validatedObject;
			User user = UserService.findByUserid(form.id);
			setMessage("validation.incorrectpassword");

			if(user != null && (user.password.equals(DigestUtils.md5Hex(form.password)))){
				return true;
			}

			return false;
		}
	}
}
