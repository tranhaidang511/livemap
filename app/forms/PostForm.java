package forms;

import play.data.validation.Check;
import play.data.validation.CheckWith;
import play.data.validation.MinSize;
import play.data.validation.Required;
import play.db.jpa.Blob;

public class PostForm {
	@Required
	@MinSize(1)
	public String message;

	@Required
	@CheckWith(DoubleValue.class)
	public String gps_lat;

	@Required
	@CheckWith(DoubleValue.class)
	public String gps_lng;
	
	@Required
	@CheckWith(ValidGenre.class)
	public String genre;

	private static class DoubleValue extends Check {
		@Override
		public boolean isSatisfied(Object validatedObject, Object value) {
			String str = (String)value;
			if(str == null || str.isEmpty()){
				return false;
			}
			try {
				Double.parseDouble(str);
				return true;
			} catch (Exception e){
				return false;
			}
		}
	}
	
	private static class ValidGenre extends Check {
		@Override
		public boolean isSatisfied(Object validatedObject, Object value) {
			PostForm form = (PostForm)validatedObject;
			try {
				int genre = Integer.parseInt(form.genre);
				if(genre < 0 || genre > 4){
					return false;
				}
				if(genre == 1){
					return form.message.matches("[0-8]");
				}
				return true;
			} catch(Exception e){
				return false;
			}
		}
	}
}