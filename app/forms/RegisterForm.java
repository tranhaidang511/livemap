package forms;

import play.data.validation.Check;
import play.data.validation.CheckWith;
import play.data.validation.Equals;
import play.data.validation.Match;
import play.data.validation.MinSize;
import play.data.validation.Required;
import play.db.jpa.Blob;
import service.UserService;

public class RegisterForm{
  @Required
  @MinSize(3)
  @Match("[\\.\\_\\-a-zA-Z0-9]+")
  @CheckWith(UserDupeCheck.class)
  public String id;

  @Required
  @MinSize(3)
  @Equals("passwordConfirmation")
  @Match("[\\.\\_\\-a-zA-Z0-9]+")
  public String password;

  @Required
  @MinSize(3)
  public String passwordConfirmation;

  static class UserDupeCheck extends Check {

    public boolean isSatisfied(Object form_, Object id) {
      RegisterForm form = (RegisterForm)form_;
      setMessage("validation.userdupe");
      return UserService.findByUserid(form.id) == null;
    }
}
}
